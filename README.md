# UnoXT Sega Master System Core

This is port of ZXUno Master System Core 

https://github.com/zxdos/zxuno/tree/master/cores/MasterSystem

Put *.sms ROMs to the SD Card

Inherited of the Ben Leperchey's Sega Master System HDL core to the ZX-UNO board.

Original source code: https://github.com/ben0109/Papilio-Master-System

All credits go to Ben. 

Ben's Blog: http://fpga-hacks.blogspot.com.es/

Ported to ZX-UNO by Quest of Zonadepruebas forum (http://www.zonadepruebas.com)



